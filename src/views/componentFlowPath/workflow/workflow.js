/* 产生随机数 */
export function getKeyRandom (y = 5) {
  let text = ''
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  for (let i = 0; i < y; i++) { text += possible.charAt(Math.floor(Math.random() * possible.length)) }
  return text
}

/* 添加条件节点 */
export function addCondition (i, _prevId, _childNode = {}) {
  const _node = {
    nodeId: 'condition_' + getKeyRandom(),
    prevId: _prevId,
    default: !i,
    name: i ? '条件' + i : '默认条件',
    nameEdit: false,
    conditions: [[]],
    type: 'condition',
    placeholder: i ? '请设置条件' : '其他条件进入此流程'
  }
  if (_childNode.nodeId) {
    _node.childNode = _childNode
  }
  return _node
}
